DROP TABLE IF EXISTS `users`;

CREATE TABLE users
(
    id int(11) NOT NULL AUTO_INCREMENT,
    name varchar(100) NOT NULL,
    email varchar(100) DEFAULT NULL,
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS `roles`;

create table roles
(
  id int(11) not null auto_increment,
  name varchar(100) not null,
  description varchar(100) not null,
  primary key(id)
);