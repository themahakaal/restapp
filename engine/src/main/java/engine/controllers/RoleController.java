package engine.controllers;

import appplication.models.domain.RoleAppModel;
import engine.services.IRoleService;
import engine.services.RoleServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class RoleController {
    @Autowired
    private IRoleService service;

    @RequestMapping("/roles")
    public List<RoleAppModel> getUsers() {
        return service.getAll();
    }

    @RequestMapping(value = "/role/{id}", method = RequestMethod.GET)
    public RoleAppModel getUser(@PathVariable Integer id) {
        return service.getRole(id);
    }

    @RequestMapping(value = "/role/create", method = RequestMethod.POST)
    public void createUser(@RequestBody RoleAppModel role) {
        this.service.create(role);
    }

    @RequestMapping(value = "/role/{id}/update", method = RequestMethod.PUT)
    public void updateUser(@PathVariable Integer id, @RequestBody RoleAppModel role) { this.service.update(role); }

    @RequestMapping(value = "/role/{id}/delete", method = RequestMethod.DELETE)
    public void deleteUser(@PathVariable Integer id) {
        this.service.delete(id);
    }
}
