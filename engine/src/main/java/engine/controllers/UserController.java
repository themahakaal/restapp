package engine.controllers;

import appplication.models.domain.UserAppModel;
import engine.domain.User;
import engine.services.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserController {
    @Autowired
    private IUserService userService;

    @RequestMapping("/users")
    public List<UserAppModel> getUsers() {
        return userService.getUsers();
    }

    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
    public UserAppModel getUser(@PathVariable Integer id) {
        return userService.getUser(id);
    }

    @RequestMapping(value = "/user/create", method = RequestMethod.POST)
    public void createUser(@RequestBody UserAppModel user) {
        this.userService.createUser(user);
    }

    @RequestMapping(value = "/user/{id}/update", method = RequestMethod.PUT)
    public void updateUser(@PathVariable Integer id, @RequestBody UserAppModel user) { this.userService.updateUser(user); }

    @RequestMapping(value = "/user/{id}/delete", method = RequestMethod.DELETE)
    public void deleteUser(@PathVariable Integer id) {
        this.userService.deleteUser(id);
        }
    }
