package engine.services;

import appplication.models.domain.UserAppModel;
import engine.domain.User;

import java.util.List;

public interface IUserService {
    List<UserAppModel> getUsers();
    UserAppModel getUser(Integer id);
    void updateUser(UserAppModel user);
    void createUser(UserAppModel user);
    void deleteUser(Integer id);
}
