package engine.services;

import appplication.models.domain.RoleAppModel;
import engine.domain.Role;

import java.util.List;

public interface IRoleService {
    List<RoleAppModel> getAll();
    RoleAppModel getRole(int id);
    void update(RoleAppModel role);
    void create(RoleAppModel role);
    void delete(int id);
}
