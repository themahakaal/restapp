package engine.services;

import appplication.models.domain.UserAppModel;
import engine.dao.IUserDao;
import engine.dao.UserDaoImpl;
import engine.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements IUserService {

    @Autowired
    UserDaoImpl userDao;

    @Override
    public List<UserAppModel> getUsers() {
        List<UserAppModel> result = new ArrayList<>();
        List<User> users = userDao.getAll();

        for (User u: users) {
            result.add(new UserAppModel(u.getId(), u.getName(), u.getEmail()));
        }

        return result;
    }

    @Override
    public UserAppModel getUser(Integer id) {
        User user = this.userDao.get(id);

        return new UserAppModel(user.getId(), user.getName(), user.getEmail());
    }

    @Override
    public void createUser(UserAppModel user) {
        User modelUser = new User(user.getName(), user.getEmail());
        this.userDao.insert(modelUser);
    }

    @Override
    public void updateUser(UserAppModel user) {
        User model_user = new User(user.getId(), user.getName(), user.getEmail());
        this.userDao.update(model_user);
    }

    @Override
    public void deleteUser(Integer id) {
        this.userDao.delete(id);
    }
}
