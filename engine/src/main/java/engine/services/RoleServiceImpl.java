package engine.services;

import appplication.models.domain.RoleAppModel;
import engine.dao.RoleDaoImpl;
import engine.domain.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RoleServiceImpl implements IRoleService {

    @Autowired
    private RoleDaoImpl roleDao;

    @Override
    public List<RoleAppModel> getAll() {
        List<Role> roles = this.roleDao.getAll();
        List<RoleAppModel> result = new ArrayList<>();

        for(Role r: roles) {
            result.add(new RoleAppModel(r.getId(), r.getName(), r.getDescription()));
        }

        return result;
    }

    @Override
    public RoleAppModel getRole(int id) {
        Role role = this.roleDao.get(id);

        return new RoleAppModel(role.getId(), role.getName(), role.getDescription());
    }

    @Override
    public void update(RoleAppModel role) {
        Role r = new Role(role.getId(), role.getName(), role.getDescription());

        this.roleDao.update(r);
    }

    @Override
    public void create(RoleAppModel role) {
        Role r = new Role(role.getId(), role.getName(), role.getDescription());

        this.roleDao.insert(r);
    }

    @Override
    public void delete(int id) {
        this.roleDao.delete(id);
    }
}
