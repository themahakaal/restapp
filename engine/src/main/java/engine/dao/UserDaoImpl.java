package engine.dao;

import engine.domain.User;
import engine.mappers.UserMapper;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserDaoImpl implements GenericDao<User> {

    @Autowired
    private UserMapper userMapper;

    @Override
    public void insert(User obj) {
        this.userMapper.insert(obj);
    }

    @Override
    public void update(User obj) {
        this.userMapper.updateByPrimaryKey(obj);
    }

    @Override
    public User get(int id) {
        return this.userMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<User> getAll() {
        return this.userMapper.selectAll();
    }

    @Override
    public void delete(int id) {
        this.userMapper.deleteByPrimaryKey(id);
    }
}
