package engine.dao;

import java.util.List;

public interface GenericDao<T> {
    void insert(T obj);
    void update(T obj);
    T get(int id);
    List<T> getAll();
    void delete(int id);
}
