package engine.dao;

import engine.domain.User;

import java.util.List;

public interface IUserDao {
    void insertUser(User user);
    User findUserById(Integer id);
    List<User> findAllUsers();
    void deleteUser(Integer id);
    void updateUser(User user);
}