package engine.dao;

import engine.domain.Role;
import engine.mappers.RoleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class RoleDaoImpl implements GenericDao<Role> {
    @Autowired
    private RoleMapper roleMapper;

    @Override
    public void insert(Role obj) {
        this.roleMapper.insert(obj);
    }

    @Override
    public void update(Role obj) {
        this.roleMapper.updateByPrimaryKey(obj);
    }

    @Override
    public Role get(int id) {
        return this.roleMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<Role> getAll() {
        return this.roleMapper.selectAll();
    }

    @Override
    public void delete(int id) {
        this.roleMapper.deleteByPrimaryKey(id);
    }
}
