package appplication.models.domain;

public class UserAppModel {
    private Integer id;
    private String name;
    private String email;

    public UserAppModel(){

    }

    public UserAppModel(int id, String name, String email) {
        this.id = id;
        this.name = name;
        this.email = email;
    }

    public UserAppModel(String _name, String _email) {
	this.name = _name;
	this.email = _email;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
