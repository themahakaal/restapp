package vaadin.view.backend;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.cookie.Cookie;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class AuthenticationService {
    private static AuthenticationService instance = null;

    private HttpRequestManager requestManager;
    private Cookie userAuthenticationCookie;

    private AuthenticationService () {
        this.requestManager = HttpRequestManager.getInstance();
        this.userAuthenticationCookie = null;
    }

    public static AuthenticationService getInstance () {
        if (instance == null) {
            instance = new AuthenticationService();
        }

        return instance;
    }

    public boolean isAuthenticated() {
        return this.userAuthenticationCookie != null;
    }

    private void getAuthenticated(String username, String password) {
        HttpResponse response = null;
        String uri = "http://localhost:8081/login";
        HttpPost request = new HttpPost(uri);

        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("username", username));
        params.add(new BasicNameValuePair("password", password));

        try {
            request.setEntity(new UrlEncodedFormEntity(params));
            response = this.requestManager.executeHttpRequest(request);

            if (response.getStatusLine().getStatusCode() == 200) {
                List<Cookie> cookies = this.requestManager.getCookieStore().getCookies();

                for(Cookie cookie: cookies) {
                    if (cookie.getName().equals("JSESSIONID")) {
                        this.userAuthenticationCookie = cookie;
                    }
                }
            }
        }  catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean authenticate(String username, String password) {
        if (!username.isEmpty() && !password.isEmpty()) {
            this.getAuthenticated(username, password);
        }

        return isAuthenticated();
    }

    public void logout() {
        this.userAuthenticationCookie = null;
        this.requestManager.getCookieStore().clear();
    }
}
