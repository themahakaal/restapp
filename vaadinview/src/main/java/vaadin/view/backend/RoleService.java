package vaadin.view.backend;

import appplication.models.domain.RoleAppModel;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpEntity;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.*;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class RoleService {

    private static RoleService roleService = null;
    private HttpClient httpClient;
    private ObjectMapper mapper;
    private static final String url = "http://localhost:8081";
    private HttpRequestManager requestManager;

    private RoleService() {
        this.httpClient = HttpClientBuilder.create().build();
        this.mapper = new ObjectMapper();
        requestManager = HttpRequestManager.getInstance();
    }

    public static RoleService getInstance() {
        if (RoleService.roleService == null) {
            RoleService.roleService = new RoleService();
        }

        return RoleService.roleService;
    }

    public List<RoleAppModel> getAll(String nameFilter) throws IOException {
        List<RoleAppModel> roles = new ArrayList<>();
        String uri = "/roles";
        HttpGet request = new HttpGet(RoleService.url + uri);

        String content = this.getJson(this.execute(request));

        if (content == null) return roles;

        JSONArray jsonArray = new JSONArray(content);

        for(int i = 0; i < jsonArray.length(); i++) {
            roles.add(this.mapper.readValue(jsonArray.getJSONObject(i).toString(), RoleAppModel.class));
        }

        return roles.stream().filter(roleAppModel -> (
                (nameFilter == null || nameFilter.isEmpty() ||
                        roleAppModel.getName().toLowerCase()
                                .startsWith(nameFilter.toLowerCase()
                                )
                )
        )).collect(Collectors.toList());
    }

    public RoleAppModel getRole(int id) throws IOException {
        String uri = "/role/" + id;
        HttpGet request = new HttpGet(RoleService.url + uri);

        String content = this.getJson(this.execute(request));

        JSONObject json = new JSONObject(content);
        return this.mapper.readValue(json.toString(), RoleAppModel.class);
    }

    public void createRole(RoleAppModel role) throws IOException {
        HttpEntity entity = new StringEntity(this.mapper.writeValueAsString(role));

        HttpPost request = new HttpPost(RoleService.url + "/role/create");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setEntity(entity);

        HttpResponse response = this.execute(request);

        if (response.getStatusLine().getStatusCode() != 200) {
            throw new IOException("Something went horribly wrong. Here is the HTTP status code " +
                    response.getStatusLine().getStatusCode()
                    + ".\nServer answered: " + response.getStatusLine().getReasonPhrase());
        }
    }

    public void updateRole(RoleAppModel role) throws IOException {
        HttpEntity entity = new StringEntity(this.mapper.writeValueAsString(role));
        HttpPut request = new HttpPut(RoleService.url + "/role/" + role.getId() + "/update");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setEntity(entity);

        HttpResponse response = this.execute(request);

        if (response.getStatusLine().getStatusCode() != 200) {
            throw new IOException("Something went horribly wrong. Here is the HTTP status code " +
                    response.getStatusLine().getStatusCode()
                    + ".\nServer answered: " + response.getStatusLine().getReasonPhrase());
        }
    }

    public void deleteRole(int id) throws IOException {
        String uri = "/role/" + id + "/delete";
        HttpDelete request = new HttpDelete(RoleService.url + uri);

        HttpResponse response = this.execute(request);

        if (response.getStatusLine().getStatusCode() != 200) {
            throw new IOException(String.format("Something went horribly wrong. Here is the HTTP status code %s." +
                    "\nServer answered: %s",
                    response.getStatusLine().getStatusCode(),
                    response.getStatusLine().getReasonPhrase())
            );
        }
    }

    private HttpResponse execute(HttpUriRequest request) throws IOException {
        return requestManager.executeHttpRequest(request);
    }


    private String getJson(HttpResponse response) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));
        String content = reader.readLine();

        return content;
    }
}
