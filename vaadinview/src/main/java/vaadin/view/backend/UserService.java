package vaadin.view.backend;

import appplication.models.domain.UserAppModel;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import elemental.json.JsonObject;
import jdk.nashorn.internal.runtime.UserAccessorProperty;
import org.apache.http.HttpEntity;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONString;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class UserService {

    private static UserService instance = null;
    private HttpClient client;
    private ObjectMapper mapper;
    private static final String url = "http://localhost:8081";
    private static HttpRequestManager requestManager;

    private UserService() {
        this.client = HttpClientBuilder.create().build();
        this.mapper = new ObjectMapper();
        requestManager = HttpRequestManager.getInstance();
    }

    public static UserService getInstance() {
        if (UserService.instance == null) {
            UserService.instance = new UserService();
        }

        return UserService.instance;
    }

    public List<UserAppModel> getUsers(String idFilter, String nameFilter, String emailFilter) throws IOException {
        List<UserAppModel> usersList = new ArrayList<>();
        HttpGet request = new HttpGet(UserService.url + "/users");
        // HttpResponse response = this.client.execute(request);
        HttpResponse response = requestManager.executeHttpRequest(request);

        BufferedReader reader = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));
        String jsonString = reader.readLine();

        JSONArray jsonArray = new JSONArray(jsonString);

        for(int i = 0; i < jsonArray.length(); i++) {
            JSONObject obj = jsonArray.getJSONObject(i);
            UserAppModel u = this.mapper.readValue(obj.toString(), UserAppModel.class);
            usersList.add(u);
        }
        return usersList.stream()
                .filter(u -> (idFilter == null || idFilter.isEmpty() || Integer.toString(u.getId()).equals(idFilter)))
                .filter(u -> (nameFilter == null || nameFilter.isEmpty() || u.getName().toLowerCase().startsWith(nameFilter.toLowerCase())))
                .filter(u -> (emailFilter == null || emailFilter.isEmpty() || u.getEmail().toLowerCase().startsWith(emailFilter.toLowerCase())))
                .collect(Collectors.toList());
    }

    public UserAppModel getUser(Integer id) throws IOException {
        HttpGet request = new HttpGet(UserService.url + "/user/" + id);
        HttpResponse response = this.client.execute(request);

        BufferedReader reader = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));
        String jsonString = reader.readLine();

        JSONObject obj = new JSONObject(jsonString);
        return this.mapper.readValue(obj.toString(), UserAppModel.class);
    }

    public void createUser(UserAppModel user) throws IOException {
        HttpResponse response = null;

        String json = this.mapper.writeValueAsString(user);

        HttpEntity entity = new StringEntity(json);

        HttpPost request = new HttpPost(UserService.url + "/user/create");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setEntity(entity);

        response = requestManager.executeHttpRequest(request);

        if (response.getStatusLine().getStatusCode() != 200) {
            throw new IOException("Something went horribly wrong. Here is the HTTP status code " + response.getStatusLine().getStatusCode() + ".\nServer answered: " + response.getStatusLine().getReasonPhrase());
        }
    }

    public void updateUser(UserAppModel user) throws IOException {
        HttpResponse response = null;

        String json = this.mapper.writeValueAsString(user);
        HttpEntity entity = new StringEntity(json);

        HttpPut request = new HttpPut(UserService.url + "/user/" + user.getId() + "/update");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setEntity(entity);

        response = requestManager.executeHttpRequest(request);

        if (response.getStatusLine().getStatusCode() != 200) {
            throw new IOException("Something went horribly wrong. Here is the HTTP status code " + response.getStatusLine().getStatusCode() + ".\nServer answered: " + response.getStatusLine().getReasonPhrase());
        }
    }

    public void deleteUser(Integer id) throws IOException {
        HttpResponse response = null;

        HttpDelete request = new HttpDelete(UserService.url + "/user/" + id + "/delete");
        response = requestManager.executeHttpRequest(request);

        if (response.getStatusLine().getStatusCode() != 200) {
            throw new IOException("Something went horribly wrong. Here is the HTTP status code " + response.getStatusLine().getStatusCode() + ".\nServer answered: " + response.getStatusLine().getReasonPhrase());
        }
    }
}
