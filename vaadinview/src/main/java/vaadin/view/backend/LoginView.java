package vaadin.view.backend;

import com.vaadin.flow.component.login.AbstractLogin;
import com.vaadin.flow.component.login.LoginForm;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;

@Route("")
public class LoginView extends VerticalLayout {
    private LoginForm login;
    private AuthenticationService authenticationService;

    public LoginView() {
        this.login = new LoginForm();
        this.authenticationService = AuthenticationService.getInstance();

        this.login.addLoginListener(e -> {
            if (this.authenticate(e)) {
                this.goHome();
            } else {
                this.login.setError(true);
            }
        });

        this.add(this.login);
        this.setHorizontalComponentAlignment(Alignment.CENTER, this.login);
    }

    private boolean authenticate(AbstractLogin.LoginEvent e) {
        return this.authenticationService.authenticate(e.getUsername(), e.getPassword());
    }

    private void goHome() {
        getUI().ifPresent(ui -> ui.navigate("home"));
    }
}
