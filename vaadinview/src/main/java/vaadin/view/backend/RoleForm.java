package vaadin.view.backend;

import appplication.models.domain.RoleAppModel;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import vaadin.view.MainView;

import java.io.IOException;

public class RoleForm extends FormLayout {

    private static RoleService service = RoleService.getInstance();
    private Binder<RoleAppModel> binder = new Binder<>(RoleAppModel.class);
    private MainView mainView;
    private FormLayout form;
    private TextField name;
    private TextField description;
    private Button saveButton;
    private Button deleteButton;
    private RoleAppModel role;

    public RoleForm(MainView _mainview) {
        this.mainView = _mainview;
        this.form = new FormLayout();
        this.name = new TextField("Name");
        this.description = new TextField("Description");
        this.saveButton = new Button("Save");
        this.deleteButton = new Button("Delete");

        this.binder.bindInstanceFields(this);

        this.saveButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        this.deleteButton.addThemeVariants(ButtonVariant.LUMO_ERROR);

        this.saveButton.addClickListener(e -> this.save());
        this.deleteButton.addClickListener(e -> this.delete());

        this.form.add(this.name, this.description, this.saveButton, this.deleteButton);
        this.form.setEnabled(false);

        add(this.form);
    }

    private void save() {
        try {
            if (this.role.getId() == null) {
                RoleForm.service.createRole(this.role);
            } else {
                RoleForm.service.updateRole(this.role);
            }
            this.mainView.updateRolesList();
            this.setRole(null);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void delete() {
        try {
            RoleForm.service.deleteRole(this.role.getId());
            this.mainView.updateRolesList();
            this.setRole(null);
            Notification.show(String.format("L'utente %s è stato eliminato correttamente.", this.role.getName()), 2000, Notification.Position.TOP_CENTER);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setRole(RoleAppModel role) {
        this.role = role;
        this.binder.setBean(this.role);
        this.name.focus();

        this.form.setEnabled(true);
    }
}
