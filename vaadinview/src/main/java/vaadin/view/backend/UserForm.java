package vaadin.view.backend;

import appplication.models.domain.UserAppModel;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.component.textfield.TextField;
import vaadin.view.MainView;

import java.io.IOException;

public class UserForm extends FormLayout {
    private static UserService userServiceInstance = UserService.getInstance();
    private MainView mainView;
    private FormLayout form;
    private TextField name;
    private TextField email;
    private Button saveButton;
    private Button deleteButton;
    private UserAppModel user;

    private Binder<UserAppModel> binder = new Binder<>(UserAppModel.class);

    public UserForm(MainView _mainView) {
        this.mainView = _mainView;
        this.name = new TextField("Name");
        this.email = new TextField("Email");
        this.saveButton = new Button("Save");
        this.deleteButton = new Button("Delete");
        this.form = new FormLayout();


        this.binder.bindInstanceFields(this);

        this.saveButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        this.deleteButton.addThemeVariants(ButtonVariant.LUMO_ERROR);

        this.saveButton.addClickListener(e -> this.save());
        this.deleteButton.addClickListener(e -> this.delete());

        this.form.add(this.name, this.email, this.saveButton, this.deleteButton);
        this.form.setEnabled(false);

        add(this.form);
    }

    private void save() {
        try {
            if (this.user.getId() == null) {
                UserForm.userServiceInstance.createUser(this.user);
            } else {
                UserForm.userServiceInstance.updateUser(this.user);
            }
            this.mainView.updateUsersList();
            this.setUser(null);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void delete() {
        try {
            UserForm.userServiceInstance.deleteUser(this.user.getId());
            this.mainView.updateUsersList();
            this.setUser(null);
            Notification.show(String.format("L'utente %s è stato eliminato correttamente.", this.user.getName()), 2000, Notification.Position.TOP_CENTER);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setUser(UserAppModel user) {
        this.user = user;

        //  Initializes all fields in the form and automatically updates the values in the domain objects as the
        //  corresponding field value changes in the UI
        this.binder.setBean(user);
        this.name.focus();

        // Enable form components
        //enableDisableComponents(true);
        this.form.setEnabled(true);
    }

    private void enableDisableComponents(boolean enable) {
        this.name.setEnabled(enable);
        this.email.setEnabled(enable);
        this.deleteButton.setEnabled(enable);
        this.saveButton.setEnabled(enable);
    }
}
