package vaadin.view.backend;

import org.apache.http.HttpResponse;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;

public class HttpRequestManager {
    private HttpClient httpClient;
    private CookieStore cookieStore;

    private static HttpRequestManager instance;

    private HttpRequestManager() {
        this.initialize();
    }

    public static HttpRequestManager getInstance() {
        if (instance == null) {
            instance = new HttpRequestManager();
        }

        return instance;
    }

    public HttpResponse executeHttpRequest(HttpUriRequest request) throws IOException {
        httpClient = HttpClientBuilder.create().setDefaultCookieStore(cookieStore).build();
        return httpClient.execute(request);
    }

    public CookieStore getCookieStore() {
        return cookieStore;
    }

    public void initialize() {
        cookieStore = new BasicCookieStore();
        httpClient = HttpClientBuilder.create().setDefaultCookieStore(cookieStore).build();
    }
}