package vaadin.view;

import appplication.models.domain.RoleAppModel;
import appplication.models.domain.UserAppModel;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.page.Viewport;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import vaadin.view.backend.*;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.PWA;

import java.io.IOException;

/**
 *
 */
@Route("home")
@PWA(name = "Home Page - User/Role CRUD", shortName = "home")
@Viewport("width=device-width, minimum-scale=1.0, initial-scale=1.0, user-scalable=yes")
public class MainView extends VerticalLayout implements BeforeEnterObserver {
    private AuthenticationService authenticationService;

    private UserService userService = UserService.getInstance();
    private RoleService roleService = RoleService.getInstance();
    private UserForm userForm;
    private RoleForm roleForm;
    private Grid<UserAppModel> userGrid;
    private Grid<RoleAppModel> roleGrid;
    private TextField filterId;
    private TextField filterName;
    private TextField filterEmail;
    private TextField filterRoleName;
    private Label formLabel = new Label();
    private Label roleFormLabel = new Label();
    private Button filterClearButton;
    private Button filterRoleClearButton;
    private Button newUserButton;
    private Button newRoleButton;

    public MainView() {
        this.authenticationService = AuthenticationService.getInstance();
        this.userForm = new UserForm(this);
        this.userGrid = new Grid<>(UserAppModel.class);
        this.userGrid.setHeight("400px");
        this.filterId = new TextField("Id");
        this.filterName = new TextField("Name");
        this.filterEmail = new TextField("E-mail");
        this.filterClearButton = new Button(VaadinIcon.CLOSE_CIRCLE.create());
        this.newUserButton = new Button("Add new User");
        this.newUserButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);

        // Get user list
        this.filterId.setPlaceholder("Look for Id");
        this.filterName.setPlaceholder("Look for Name");
        this.filterEmail.setPlaceholder("Look for Email");

        this.filterId.setValueChangeMode(ValueChangeMode.EAGER);
        this.filterName.setValueChangeMode(ValueChangeMode.EAGER);
        this.filterEmail.setValueChangeMode(ValueChangeMode.EAGER);

        this.filterId.addValueChangeListener(e -> this.updateUsersList());
        this.filterName.addValueChangeListener(e -> this.updateUsersList());
        this.filterEmail.addValueChangeListener(e -> this.updateUsersList());

        this.filterClearButton.addClickListener(e -> {
            this.filterId.clear();
            this.filterName.clear();
            this.filterEmail.clear();
        });

        this.updateUsersList();

        // Users update and create new users
        this.userGrid.asSingleSelect().addValueChangeListener(event -> {
            this.formLabel.setText("Edit Form");
            this.userForm.setUser(event.getValue());
        });
        this.newUserButton.addClickListener(e -> {
            this.formLabel.setText("Create Form");
            this.userForm.setUser(new UserAppModel());
        });

        // Set layout
        HorizontalLayout filtering = new HorizontalLayout(this.filterId, this.filterName, this.filterEmail, this.filterClearButton);
        VerticalLayout usersList = new VerticalLayout(this.newUserButton, filtering, this.userGrid);
        VerticalLayout usersListUpdate = new VerticalLayout(this.formLabel, this.userForm);
        HorizontalLayout layout = new HorizontalLayout(usersList, usersListUpdate);

        // Add layout to view
        add(layout);
        setSizeFull();

        // roles
        this.roleForm = new RoleForm(this);
        this.roleGrid = new Grid<>(RoleAppModel.class);
        this.roleGrid.setHeight("400px");
        this.filterRoleName = new TextField("Role");
        this.filterRoleClearButton = new Button(VaadinIcon.CLOSE_CIRCLE.create());
        this.newRoleButton = new Button("Add new Role");
        this.newRoleButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);

        this.filterRoleName.setPlaceholder("look for role name");
        this.filterRoleName.setValueChangeMode(ValueChangeMode.EAGER);
        this.filterRoleName.addValueChangeListener(e -> this.updateRolesList());

        this.updateRolesList();

        this.filterRoleClearButton.addClickListener(e -> {
          this.filterRoleName.clear();
        });

        this.roleGrid.asSingleSelect().addValueChangeListener(event -> {
            this.roleFormLabel.setText("Edit form");
            this.roleForm.setRole(event.getValue());
        });

        this.newRoleButton.addClickListener(e -> {
            this.roleFormLabel.setText("Create new Role");
            this.roleForm.setRole(new RoleAppModel());
        });

        // create layout
        HorizontalLayout filter = new HorizontalLayout(this.filterRoleName, this.filterRoleClearButton);
        VerticalLayout roleList = new VerticalLayout(this.newRoleButton, filter, this.roleGrid);
        VerticalLayout roleListUpdate = new VerticalLayout(this.roleFormLabel, this.roleForm);
        HorizontalLayout layout1 = new HorizontalLayout(roleList, roleListUpdate);

        add(layout1);
        setSizeFull();
    }

    public void updateUsersList() {
        try {
            this.userGrid.setItems(this.userService.getUsers(
                    this.filterId.getValue(), this.filterName.getValue(), this.filterEmail.getValue()
            ));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void updateRolesList() {
        try {
            this.roleGrid.setItems(this.roleService.getAll(this.filterRoleName.getValue()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void beforeEnter(BeforeEnterEvent beforeEnterEvent) {
        if (!this.authenticationService.isAuthenticated()) {
            beforeEnterEvent.rerouteTo(LoginView.class);
        }
    }
}